import React from 'react'
import { Link } from 'react-router-dom'; 

const SideBarItemsLists = ({ heading, items }) => {
 
    return (
        <div>
            {heading && <h1 className='font-semibold text-lg my-4 mx-4 '>{heading}</h1>}
            <ul className='border-b-2 pb-4 '>
                {
                    items.map((item, idx) => {
                        return (
                            <div key={idx} className='flex items-center cursor-pointer px-3 py-2 hover:bg-gray-100 text-black rounded-md w-full'>
                                {item.icon}
                                <li className='ml-4'><Link to="/">{item.name}</Link></li>
                            </div>
                        )
                    })
                }

            </ul>
        </div>
    )
}

export default SideBarItemsLists