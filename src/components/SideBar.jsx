import React from 'react'
import { useSelector } from 'react-redux'
import SideBarItemsLists from './SideBarItemsLists';
import { FaFire } from "react-icons/fa6";
import { MdOutlineShoppingBag, MdScience, MdOutlineSubscriptions } from "react-icons/md";
import { IoIosMusicalNote, IoMdHome } from "react-icons/io";
import { PiFilmSlate } from "react-icons/pi";
import { HiOutlineStatusOnline } from "react-icons/hi";
import { IoGameControllerSharp } from "react-icons/io5";
import { IoNewspaper, IoWineOutline } from "react-icons/io5";
import { GiClothes } from "react-icons/gi";
import { LuPodcast } from "react-icons/lu";
import { CiUser } from "react-icons/ci";
import { LiaHistorySolid } from "react-icons/lia";
import { GoVideo } from "react-icons/go";
import { MdAccessTime } from "react-icons/md";
import { TbVideoMinus } from "react-icons/tb";


const home = [
  {
    name: "Home",
    icon: <IoMdHome size="24px" />
  },
  {
    name: "Short",
    icon: <TbVideoMinus size="24px" />,
  },
  {
    name: "Subscription",
    icon: <MdOutlineSubscriptions size="24px" />
  },
]

const you = [
  {
    name: "Your Channel",
    icon: <CiUser size={"24px"} />
  },
  {
    name: "History",
    icon: <LiaHistorySolid size={"24px"} />
  },
  {
    name: "Your Videos",
    icon: <GoVideo size={"24px"} />
  },
  {
    name: "Watch Later",
    icon: <MdAccessTime size={"24px"} />
  },
]

const explore = [
  {
    name: "Trending",
    icon: <FaFire size={"24px"} />
  },
  {
    name: "Shopping",
    icon: <MdOutlineShoppingBag size={"24px"} />
  },
  {
    name: "Music",
    icon: <IoIosMusicalNote size={"24px"} />
  },
  {
    name: "Films",
    icon: <PiFilmSlate size={"24px"} />
  },
  {
    name: "Live",
    icon: <HiOutlineStatusOnline size={"24px"} />
  },
  {
    name: "Gaming",
    icon: <IoGameControllerSharp size={"24px"} />
  },
  {
    name: "News",
    icon: <IoNewspaper size={"24px"} />
  },
  {
    name: "Sport",
    icon: <IoWineOutline size={"24px"} />
  },
  {
    name: "Learning",
    icon: <MdScience size={"24px"} />
  },
  {
    name: "Fashion & Beauty",
    icon: <GiClothes size={"24px"} />
  },
  {
    name: "Podcast",
    icon: <LuPodcast size={"24px"} />
  }
];

const SideBar = () => {
    const isMenuOpen = useSelector((state) => state.app.isMenuOpen);


  return !isMenuOpen ? null : (
    // <div className='absolute left-0 md:relative w-auto p-5  md:w-[20%] h-[calc(100vh-4.625rem)] overflow-y-scroll overflow-x-hidden bg-white  transition-all duration-500'>
    <div className='w-64  h-full border-r border-gray-200 fixed left-0 mt-16 bg-white text-black overflow-y-scroll overflow-x-hidden '>
      <SideBarItemsLists heading={null} items={home} />
      <SideBarItemsLists heading={"You"} items={you} />
      <SideBarItemsLists heading="Explore" items={explore} />
    </div>
  )
}

export default SideBar